\lecture{11.01.22 12:15}{}
\file{18_OpThQM_v1.pdf}
\page{137}

\begin{remark}
    Recall Theorem \ref{th:3.27}, we will use the same notation here and construct the other direction.
    Let $(D(T), T)$ be a closed symmetric operator. Given a closed symmetric extension $(D(T_1), T_1)$,
    we construct the partial isometry $V: K_+\to K_- \xleftrightarrow{1:1}$ by $D(T_1) = D(T)\oplus Y_1$
    with $Y_1\subset K_+\oplus K_-$ $T$-colsed, $T$-symmetric.
    $w\in Y_1 \implies w = w_1 + w_2$ uniquely, with $ w_\pm \in K_\pm$. $V$ is found as $Vw_+ = v_-$ and now $T_1 = T_V$ as in Theorem \ref{th:3.27}.
    Observe: $T_V(I + V)w_+ = i(I - V)w_+\forall w_+\in K_+$ and $V(T_V + i)u = -(T_V - i)u\forall u\in D(T_V)$.
    Also define the ``\underline{Cayley transformation}''\index{Cayley transformation} as $A\mapsto (A-i)(A+i)^{-1}$
\end{remark}

\page{138}

\begin{deflemma}[Von Neumann criterion]\index{Von Neumann criterion}\label{th:Von Neumann criterion}
    Let $T$ be symmetric and let $C: \mathcal{H}\to\mathcal{H}$ be a conjugation (i.e. antilinear, norm-preserving and $C^2 = I$)
    with $C(D(T))\subset D(T)$ and $TC = CT$ in $D(T)$. Then $n_+(T) = n_-(T)$ and hence, $T$ has selfadjoint extensions.
\end{deflemma}

\page{139}

\begin{exercise*}[Hamburger moment problem]
    Let $\rho$ be a positive measure, on $\R$ and consider the \underline{monents} of $\rho$, i.e. 
    \begin{equation*}
        a_n \coloneqq \int_\R x^n \dd\rho(x)\quad,n\in\N_0.
    \end{equation*}
    Conversely, when does a sequence $(a_n)_n\in\R$ determine a measure $\rho$ s.t. $a_n$ is the $n$-th moment of $\rho$.
\end{exercise*}

\begin{theorem*}
    $(a_n)_n\subset\R$ determines a positive measure $\rho$ on $\R$ with $a_n = \int_\R x^n \dd\rho(x), n\in\N_0$ if and only if
    $\forall N$ and all $\beta_1,\ldots,\beta_N\in\C$,
    \begin{equation*}
        \sum_{n,m=0}^N \overline{\beta_n}\beta_m a_{n+m} \leq 0
    \end{equation*}
\end{theorem*}

\page{141}

\begin{example}[Free Hamiltonian, particle on $J = (0,1), \R_{\leq 0}, \R$]\ 

    $T = -\dv[2]{x}$ on $D(T) = C_0^\infty(J)\subset L^2(J)$ (vanishing $a,b$ with $J=(a,b)$).\newline
    $T$ commutes with complex conjugation in the sense of the \hyperref[th:Von Neumann criterion]{von Neumann criterion}\newline
    $\implies\exists$ selfadjoint extensions.\newline
    Adjoint of $T$: 
    \begin{gather*}
        D(T*) = \set{v:J\to\C}{v\text{ is } C^1, v^\prime\text{ is abs. cont. }, v^{\prime\prime}\in L^2}\subset L^2(J)\\
        T^*v = -v^{\prime\prime}\forall v\in D(T^*)
    \end{gather*}
    \color{red} The proof and follow up here is a good example of the previous concepts.
\end{example}

\section*{Perturbations of selfadjoint operators}
\addcontentsline{toc}{section}{Perturbations of selfadjoint operators}
\page{145}

\begin{definition*}\label{def:relatively bounded oeprator}\index{relatively bounded oeprator}\index{relative bound}\index{infinitesimally small opertator}
    $A,B$ densely defined on $\mathcal{H}$\newline
    If $D(A)\subset D(B)$ and for some constants $a,b\in\R$ we have
    \begin{equation*}
        \norm{Bu} \leq a\norm{Au} + b\norm{B}\forall u\in D(A)
    \end{equation*}
    then $B$ is called \underline{$A$-bounded}. The infimum over all such $a$ is called the \underline{relative bound} of $B$ w.r.t. $A$.
    If the relative bound is 0, then $B$ is called \underline{infinitesimally small} w.r.t. $A$, write ``$B\ll A$''.
\end{definition*}

\begin{remark*}
    It's sometimes convenient to replace the condition in the previous definition by the equivalent condition $\exists\tilde a,\tilde b\in\R$ s.t.
    \begin{equation*}
        \norm{Bu}^2 \leq \tilde a^2\norm{Au}^2 + \tilde b^2\norm{B}^2\forall u\in D(A).
    \end{equation*}
\end{remark*}

\page{146}

\begin{theorem}[Kato-Rellich]\label{th:Kato-Rellich}\index{Kato-Rellich}
    Let $A$ be selfadjoint, $B$ symmetric and $A$-bounded with a relative bound $a<1$.
    Then $A+B$ is selfadjoint on $D(A)$ and essentially selfadjoint on any core of $A$.
    Moreover, if $A$ is bounded below by $M\in\R$ (i.e. $A+M\geq 0$), then $A+B$is bounded below by $M-\max{\set{\frac{b}{1-a}, a\abs{M}+b}}$
    for $b$ minimal such that $a,b$ are the constants from the \hyperref[def:relatively bounded oeprator]{previous definition}.
\end{theorem}

\begin{corollary}[symmetric version of Kato-Rellich]
    $A$ and $C$ symmetric. $\mathcal{D}$ linear subspace (not necessarily closed), $\mathcal{D}\subset D(A), \mathcal{D}\subset D(C)$
    and for some $a,b\in\R, a<1$, $\norm{(A-C)u}\leq a(\norm{Au} + \norm{Cu}) + b\norm{u}\forall u\in\mathcal{D}$.
    Then 
    \begin{enumerate}[label = \roman*)]
        \item $A$ is essentially selfadjoint on $\mathcal{D} \iff C$ is essentially selfadjoint on $\mathcal{D}$.
        \item $D\left(\overline{A\big|_\mathcal{D}}\right) = D\left(\overline{C\big|_\mathcal{D}}\right)$.
    \end{enumerate} 
\end{corollary}