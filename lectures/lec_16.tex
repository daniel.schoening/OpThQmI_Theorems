\lecture{17.12.21 12:15}{}
\file{16_OpThQM.pdf}
\page{117}

\begin{proposition}
    Let $(D(T), T)$ be a selfadjoint operator on $\mathcal{H}$.
    Then there is a unique map $\widehat{\Phi}: M_b(\R)\to \mathcal{B}(\mathcal{H})$ s.t.
    \begin{enumerate}[label = \roman*)]
        \item $\widehat{\Phi}$ is a $*$-algebra homomorphism.
        \item $\widehat{\Phi}$ is continuous, $\opnorm{\widehat{\Phi}(h)} \leq \norm{h}_\infty$.
        \item Let $(h_n)_N\subset M_b$ with $h_n(t)\to t (n\to\infty)$ for each $t$ and  $\abs{h_n(t)}\leq \abs{t}$.
        Then for any $u\in D(T), \widehat{\Phi}(h_n)u \to Tu$ in $\mathcal{H}$.
        \item Let $(h_n)_n\subset M_b, h_n(x) \to h(x) \forall x$ and $(h_n)_n$ uniformly bounded.
        Then $\widehat{\Phi}(h_n)\to \widehat{\Phi}(h)$ strongly.
    \end{enumerate}
\end{proposition}

\page{118}

\begin{remark*}
    We moreover have:
    \begin{enumerate}[label = \roman*)]
        \setcounter{enumi}{3}
        \item If $Tu = \lambda u$, then $\widehat{\Phi}(h)(u) = h(\lambda)(u)$.
        \item If $h\leq 0$, then $\widehat{\Phi}\leq 0$.
    \end{enumerate}
\end{remark*}

\begin{defremark}\label{th:3.21}\index{projection valued measure}\index{spectral measure}
    Let $M\subset\R$ be measurable. Then the operator $E_M\coloneqq \upchi_M(T)$ has the following properties:
    \begin{enumerate}[label =\roman*)]
        \item $E_M$ is an orthogonal projection.
        \item $E_\emptyset = 0, E_\R = I$.
        \item For $M = \bigcup M_j, M_j\subset\R$ measurable $\forall j, M_i\cap M_j = \emptyset$ for $i\neq j$,
        $E_M$ is the weak limit of $\sum_{j=1}^\infty E_{M_j}$.
        \item $E_ME_N = E_{M\cap N}$ for $M,N\subset\R$ measurable.
    \end{enumerate}
    A Family with these properties is called a \underline{projection valued measure} or \underline{spectral measure}.
\end{defremark}

\page{119}

Observe: for {\color{red} (I can't decipher this word)} $u\in\mathcal{H}, \inner{u}{E_Mu}$ is a (Borel) measure on $\R$  denoted $\dd \inner{u}{E_Mu}$
(as in the compactly supported case before) and by \emph{polarization} we get $\dd \inner{u}{E_Mv}$ for $u,v\in\mathcal{H}$\newline
For $h\in M_b$ define $\inner{u}{h(T)u}\coloneqq \int_\R h(\lambda) \dd \inner{u}{E_\lambda u}$.
The map $h\mapsto h(T)$ satisfies \ref{th:3.21}, hence for $T = U^{-1}M_fU$ we have $h(T) = U^{-1}M_{h\circ f}U$.

For $g$ (unbounded) complex valued measurable, set $D_g\coloneqq\set{u\in\mathcal{H}}{\int\abs{g(\lambda)}^2 \dd \inner{u}{E_\lambda u}}$.
$D_g$ is dense in $\mathcal{H}$ and  defines an (unbounded) operator $(D_g, g(T))$ on $\mathcal{H}$.
Symbolically: $g(T) = \int g(\lambda) \dd E_\lambda$. In particular, for $g(t) = t$ we write $T = \int \lambda \dd E_\lambda$ and hence:

\page{120}
\begin{theorem}
    Using $T = \int \lambda \dd P_\lambda$, we have
    \begin{equation*}
        \set{T\text{ selfadjoint on }\mathcal{H}} \xleftrightarrow{\text{1:1}} \text{Projection valued measures }\set{E_M}\text{ on }\mathcal{H}\text{ (spectral measures).}
    \end{equation*}
    For $g$ measurable, real-valued, $g(T) = \int g(\lambda) \dd P_\lambda$ on $D_g$ is self adjoint.\newline
    For $g\in M_b, g(T) = \widehat{\Phi}(g)$.
\end{theorem}

\begin{proposition}\label{th:3.23}
    Let $(D(T), T)$ be selfadjoint on $\mathcal{H}$. Then $(\Omega, \mu)$ can be chosen such that   $T = U^{-1}M_fU, M_f$ on $D{M_f}\subset L^2(\mu)$
    with $f\in L^p(\mu), 1\leq p\leq \infty$.
\end{proposition}

\page{121}

\begin{corollary*}\index{core}
    Any $\mathcal{D}\subset L^q(\Omega, \dd \mu)$ where $\frac1q + \frac1p = \frac12$ is a \underline{core} ($\overline{S\big|_\mathcal{D}} = S$ for $S$ closed)
    for $M_f$ as in \ref{th:3.23}, where $f\in L^p(\Omega, \dd \mu)$ (for any $2<p<\infty$).
\end{corollary*}

\begin{remark*}
    This is a rather nice way to understand selfadjoint operators without constructing the domain, since a core is sufficient.
\end{remark*}

\page{122}
\section*{Stone-von Neumann Theorem}
\addcontentsline{toc}{section}{Stone-von Neumann Theorem}

\begin{proposition}
    Let $(D(A), A)$ be selfadjoint on $\mathcal{H}$. Define $U(t)\coloneqq e^{itA}$ for $t\in\R$ by functional calculus.\newline
    Then $(U(t))_{t\in\R}$ is a \underline{strongly continuous, one-parameter group of unitaries}, i.e.:
    \begin{enumerate}[label =\roman*)]
        \item $U(t)$ is unitary for any $t\in\R$. $U(t+g) = U(t)U(g)$ (group law).
        \item For $v\in\mathcal{H}, t_n\to t_\infty$ in $\R$, $U(t_n)v\to U(t_\infty)v$ (strong continuity of $t\mapsto U(t)$).
    \end{enumerate}
    Moreover:
    \begin{enumerate}
        \setcounter{enumi}{2}
        \item For $v\in D(A)$, we have $\frac{1}{t} (U(t)v - v)\to iAv$ (as $t\to0$).
        \item If $\lim_{t\to\infty}\frac{1}{t} (U(t)v - v)$ exists, then $v\in D(A)$.
    \end{enumerate}
\end{proposition}

\page{123}
\begin{theorem}[Stone-von Neumann Theorem]\index{Stone-von Neumann Theorem}
    Let $(U(t))_{t\in\R}$ be a strongly continuous one-parameter group of unitaries.
    Then there is a self-adjoint operator $A$ on $\mathcal{H}\st U(t) = e^{itA}$. 
\end{theorem}

\newpage