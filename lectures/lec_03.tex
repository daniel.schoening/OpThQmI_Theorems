\chapter{Bounded Spaces}

\lecture{02.11.21 12:15}{}
\file{03_OpTh_v1.pdf}
\page{17}


\begin{definition}\index{bounded linear operators $\mathcal{B}(\mathcal{H})$}\index{operator norm}
    The space of bounded linear functions on $\mathcal{H}$ is denoted as $\mathcal{B}(\mathcal{H})$,
    i.e. $\mathcal{B}(\mathcal{H}) \coloneqq \set{T\colon\mathcal{H}\to\mathcal{H}}{T\text{ is linear, bounded}}$\newline
    The \underline{operator norm} is defined as
    \begin{align*}
        \opnorm{T} 
        &= \sup_{u\neq 0}\frac{\norm{Tu}}{\norm{u}}\\
        &= \sup_{\norm{u}=1}\norm{Tu}\\
        &= \inf\set{C\in R}{\norm{Tu}\leq C\norm{u}\forall u\in\mathcal{H}}\\
        &= \sup\set{\abs{\inner{u}{Tv}}}{u\in D, \norm{u}=1, v\in \tilde D, \norm{v}=1}\qq{for any $D,\tilde D$ dense in $\mathcal{H}$}
    \end{align*}
\end{definition}

\page{18}

\begin{example}\
    \begin{itemize}
        \item Let $\mathcal{M}\subset\mathcal{H}$ be a subspace,
        with $P_\mathcal{M}\colon \mathcal{H}\to\mathcal{H}, P_\mathcal{M}(u) = u_\mathcal{M}$ 
        where $u = u_\mathcal{M} + u_{\mathcal{M}\orth}$.
        \item Let $\mathcal{H} = \ell^2(\N)$ with ONB $\set{e_n} = \set{\left(\delta_{n,m}\right)_m}$.
        Let furthermore $(a_n)_n\subset\complexes$ be a bounded sequence,
        then using $x = \sum x_ne_n$ the operator $Tx \coloneqq \sum a_nx_ne_n$ is linear and bounded,
        and thus in $\mathcal{B}(\mathcal{H})$ with $\opnorm{T} = \sup{(a_n)}$.
        \item The operator $X\colon L^2(\R)\to X(L^2(\R)), u \mapsto xu$ is not bounded and in general not even in $L^2(\R)$.
    \end{itemize}
\end{example}

\page{19}

\begin{observe*}
    The operator does not in general assume the operator norm,
    i.e. it is possible that $\nexists x\in\mathcal{H}\st\opnorm{T}=\norm{Tx}$.
\end{observe*}

\begin{definition*}\index{adjoint of bounded operator}
    Let $T\in \mathcal{B}(\mathcal{H})$, the \underline{adjoint} $T^*$ is defined as $\forall u,v\in\mathcal{H},\inner{u}{Tv} = \inner{T^*u}{v}$.
\end{definition*}

\page{20}

\begin{remark}
    $\mathcal{B}(\mathcal{H})$ is an algebra, i.e. its $\forall T,S\in\mathcal{B}(\mathcal{H})$
    \begin{itemize}
        \item linear: $\opnorm{\lambda T + \mu S} \leq \abs{\lambda}\opnorm{T} + \abs{\mu}\opnorm{S}\leq\infty$
        \item closed under multiplication: $\opnorm{TS} \leq \opnorm{T}\opnorm{S}$
        \item $\mathcal{B}(\mathcal{H})$ is a *-algebra, i.e. caries an involution: $(TS)^* = S^*T^*, {T^*}^* = T$.
    \end{itemize}
\end{remark}

\page{21}

\begin{remark*}
    $\opnorm{T^*T} = \opnorm{TT^*} = \opnorm{T}^2$
\end{remark*}

\begin{definition*}\index{selfadjoint bounded operator}
    $T\in\mathcal{B}(\mathcal{H})$ is \underline{selfadjoint} iff. $T^* = T$.
\end{definition*}

\page{22}

\begin{defremark}\index{convergence on $\mathcal{B}(\mathcal{H})$}
    Let $\set{T_n}_n\subset\mathcal{B}(\mathcal{H})$
    \begin{itemize}
        \item $T_n \to T$ \underline{uniformly} (in norm, in the uniform topology) if $\opnorm{T_n - T}\to 0, n\to\infty$
        \item $T_n \to T$ \underline{strongly} (in the strong topology) if $\forall u\in\mathcal{H}\opnorm{T_nu - Tu}\to 0, n\to\infty$
        \item $T_n \to T$ \underline{weakly} (in the weak topology) if $\forall u\in\mathcal{H} T_nu \to Tu\text{ weakly, }n\to\infty$ i.e
        $\inner{v}{T_nu} - \inner{v}{Tu} \to 0, n\to \infty\forall v\in\mathcal{H}$.
    \end{itemize}
    Uniform convergence $\implies$ strong convergence $\implies$ weak convergence.\newline
    Strong operator topology: weakest topology s.t. $\forall u\in\mathcal{H},\ E_u: T \mapsto Tu$ is continuous.\newline
    Strong operator topology: weakest topology s.t. $\forall u\in\mathcal{H}, \varphi\in\mathcal{H}^*,\ E_{u,\varphi}: T \mapsto \varphi(Tu)$ is continuous.\newline
\end{defremark}

\page{23}

\begin{proposition}[continouity of multiplication]
    Let $\set{T_n}_n, \set{S_n}_n\subset \mathcal{B}(\mathcal{H}), T_n\to T$ strongly/uniformly, $S_n \to S$ strongly/uniformly.
    Then $T_nS_n\to TS$ strongly/uniformly.
\end{proposition}

\begin{proposition}
    $\mathcal{B}(\mathcal{H})\ni T\mapsto T^*$ is continuous in the weak and uniform operator topologies,
    but continuous in the strong operator topology \underline{only} if $\dim{\mathcal{H}}<\infty$.
\end{proposition}

\page{24}

\begin{proposition}[weak completeness]\index{weak completeness}
    Let $\set{T_n}_n\subset \mathcal{B}(\mathcal{H})$. 
    Let furthermore for all $u,v\in\mathcal{H}, \set{\inner{v}{T_nu}}_n$ converge (to $c_{u,v}\in\complexes$) for $n\to\infty$. 
    Then $\exists T\in \mathcal{B}(\mathcal{H})\st T_n\to T$ weakly for $n\to\infty$. 
\end{proposition}

\page{25}

\begin{proposition}[corollary to Riesz]
    Let $B$ be a bounded, sesquilinear form, then $\exists T\in \mathcal{B}(\mathcal{H})\st B(u,v) = \inner{Tu}{v} \forall u,v\in\mathcal{H}$
\end{proposition}

\begin{proposition}[Von Neumann series]\index{Von Neumann series}
    Let $T\in \mathcal{B}(\mathcal{H}), \opnorm{T}<1$, then $I-T$ is invertible in $\mathcal{B}(\mathcal{H})$,
    in particular $\range{I-T} = \mathcal{H}$ and $(I-T)^{-1}\in \mathcal{B}(\mathcal{H})$.
    Moreover, 
    \begin{equation*}
        (I-T)^{-1} = \sum_{k=0}^\infty T^k \qq{and} \opnorm{(I_T)^{-1}}\leq\frac{1}{1-{\opnorm{T}}},
    \end{equation*}
    which converges w.r.t $\opnorm{\cdot}$.
\end{proposition}