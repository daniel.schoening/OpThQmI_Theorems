\chapter{Some topics in quantum mechanics}

\lecture{21.01.22 12:15}{}
\file{21_OpThQM_v1.pdf}

\section*{Schrödinger equation with time independent potential}
\addcontentsline{toc}{section}{Schrödinger equation with time independent potential}
\page{161}

\begin{notation*}
    $J\subset\R$ interval, $\mathcal{H}$ Hilbert space and $X\subset\mathcal{H}$ subspace (closed or not). Set 
    \begin{equation*}
        C^k(J,X) \coloneqq \set{J\ni t\mapsto u(t) = u^t\in X}{J\ni t\mapsto \dv[j]{t} u^t\text{ exists and is cont. for } j = 0, 1, \ldots, kS}
    \end{equation*}
\end{notation*}

\begin{remark}\

    \begin{enumerate}[label = \roman*)]
        \item $C^k(J, X)$ is a vector space.
        \item $\dv{t} \inner{u^t}{v^t} = \inner{\dv{t} u^t}{v^t} + \inner{u^t}{\dv{t} v^t}\forall t\in J$, for $T\mapsto u^t, t\mapsto v^t$ differentiable.
        \item\label{th:4.1iii} Let $\Omega\subset\R^n$ be bounded and open, $J$ an open interval and $\set{u^t}_{t\in J}\subset\mathcal{L}^2(\Omega, \dd x)\quad$
        ($\mathcal{L}^2$ is $L^2$ before completion).
        If $t\mapsto u^t(x)$ are differentiable in $t$ for all $x\in\Omega$ and $\abs{\pd_t u^t(x)}\leq C\forall t\in J$,
        \page{162}
        then $\dv{u^t}{t}$ exists for all $t\in J$ and $\dv{u^t}{t} = \pd_t u^t$ for almost all x and for all $t\in J$ and $\dv{u^t}{t}\in L^2(\Omega, \dd x)$
    \end{enumerate}
\end{remark}

\begin{definition*}[PDE-version of the Schrödinger equation]
    \index{Schrödinger equation}\index{classical solution to the Schrödinger equation}\index{abstract Schrödinger equation}\index{homogenous Schrödinger equation}
    \begin{equation*}
        -i\pd_tu^t(x) + (A_0u^t)(x) = S(t,x)
    \end{equation*}
    Where $A_0\coloneqq -\Delta_x + V(x)$ on $L^2(\Omega, \dd x)$ with domain $D(A_0)\subset C^2(\Omega)\subset L^2(\Omega, \dd x)$ is the Hamiltonian,
    $\Omega\subset\R^n$ open, $V: \Omega\to\R$, $S:J\times \Omega \to \C$ continuous is the source term and $J\subset\R$ an interval.\par\page{163}
    A function on $J\times \Omega$ is called a \underline{classical solution} if it is of class $C^1$ in $t$ and $C^2$ in $x$
    and solves the Schrödinger equation (in particular, for any $t\in J$ fixed, $u(t,\cdot)\in D(A_0)$).\par
    Assume $V$ is such that $A_0$ is essentially selfadjoint and consider $A \coloneqq \overline{A_0}$ then the \underline{abstract Schrödinger equation}
    is given as
    \begin{equation*}
        -i\dv{t} u^t + Au^t = S^t
    \end{equation*} 
    with $J\subset\R$ interval, $t\mapsto S^t$ continuous on $J$, and we are looking for $u\in C^1(J,D(A))$.
    If $S^t=0$, then it is called the \underline{homogenous equation}.\par 
    The \underline{Cauchy} problem for the abstract Schrödinger equation is finding a solution $u$ in $C^1(J,D(A))$ that satisfies the initial condition 
    $u^0 = v\in D(A)$.    
\end{definition*}

\begin{remark*}
    Under conditions such as in \hyperref[th:4.1iii]{remark 4.1 iii)}, a classical solution $u(t,x)$ with $u(t,\cdot)\in D(A_0)\forall t\in J$
    is a solution of the abstract Schrödinger equation.\newline (since $D(A)\subset D(A_0)$).
\end{remark*}

\page{164}

\begin{proposition}
    Let $U:t\mapsto u^t\in C^1(J, D(A))$ be a solution to the abstract homogeneous Schrödinger equation.
    Then $\norm{u^t} = \norm{u^0}$. Hence, the Cauchy problem (also for $S^t=0$) has unique solution.
\end{proposition}

\begin{proposition}
    \begin{equation*}
        u^t \coloneqq e^{-itA}v, t\in J
    \end{equation*}
    is a solution to the homogeneous Cauchy problem with initial condition $v\in D(A)$.
\end{proposition}

\begin{remark*}
    By uniqueness, this is the only solution.
\end{remark*}

\begin{remark*}\index{weak solution to the Schrödinger equation}
    $e^{-itA}v$ is defined for any $v\in\mathcal{H}$, not just $v\in D(A)$.\newline If $v\not\in D(A), t\mapsto u^t\coloneqq e^{-itA}v$ does \underline{not} 
    solve the Schrödinger equation, but:
    \begin{equation*}
        \dv{t} \inner{w}{u^t} + i\inner{Aw}{u^t} = 0 \forall w\in D(A), t\in J
    \end{equation*}
    (since $\inner{w}{e^{-itA}v} = \inner{e^{itA}w}{v}$) and $u^t$ is called a \underline{weak solution}.
\end{remark*}

\newpage

By a variation of variation argument, we get the solutions of the inhomogeneous equation:

\begin{theorem}\label{th:4.4}
    Let $(A, D(A))$ be a selfadjoint operator on the Hilbert space $\mathcal{H}$ Let $J\subset\R$ be an open interval containing zero.
    Let $v\in D(A), t\mapsto S^t$ be continuous $J\to\mathcal{H}$ and $S^t\in D(A)\forall t\in J$ and $t\mapsto AS^t$ be continuous $J\to \mathcal{H}$.\newline
    Then there is a unique solution $u:t\mapsto u^t\in C^1(J, D(A))$ to the Cauchy problem 
    \begin{equation*}
        \begin{cases}
            i\dv{t}u^t + Au^t = S^t\\
            u^0=v,
        \end{cases}
    \end{equation*}
    which has the form 
    \begin{equation*}
        u^t = e^{-itA}v + e{-itA}\int_0^t ie^{isA}S^s \dd s.
    \end{equation*}
    \page{166}
    The $\mathcal{H}$ valued Integral is defined as in Stone's Theorem {\color{red}(probably)} \ref{th:3.17}:\newline
    For $J\ni t\mapsto B^t \in \mathcal{B}(\mathcal{H})$ continuous in the strong sense,
    $j\ni t\mapsto \varphi^t\in \mathcal{H}$ continuous, then for $a,b\in J$ the vector $\int_a^b B^s\varphi^s \dd s\in \mathcal{H}$  is
    the unique vector in $\mathcal{H}$ such that $\inner{w}{\int_a^b B^s\varphi^s \dd s} = \int_a^b \inner{w}{B^s\varphi^s} \dd s\forall x\in\mathcal{H}$.
\end{theorem}

\page{170}

\begin{example*}
    Set $S^t \coloneqq e^{\alpha t}w, w\in D(A)\subset\mathcal{H}, \alpha\in\R\setminus\zeroset$.
    Then the Cauchy problem in \ref{th:4.4} is solved by 
    \begin{equation*}
        u^t = e^{-ita}v + \underbrace{\left(A-i\alpha I\right)^{-1}}_{R(i\alpha, A)\in \mathcal{B}(\mathcal{H})} \left(e^{-ita}-e^{\alpha t}\right)w
    \end{equation*}
\end{example*}