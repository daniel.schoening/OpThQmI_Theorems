\section*{Sturm-Liouville operators}
\addcontentsline{toc}{section}{Sturm-Liouville operators}

\lecture{01.02.22 12:15}{}
\file{24_OpThQM_bak.pdf}
\page{189}

\begin{definition*}\index{Sturm-Liouville operator}
    The unbound operator
    \begin{equation*}
        A = \frac{1}{r(x)}\left(-\dv{x} p(x) \dv{x} + q(x)\right)
    \end{equation*}
    on $\mathcal{H} = L^2(I, r(x)\dd x)$ with $I$ an interval $(a,b)$, $\frac{1}{p}\in L^1_{loc}(I)$ positive, 
    $q\in L^1_{loc}(I)$ real-valued and $r\in L^1_{loc}(I)$ positive is called a \underline{Sturm-Liouville operator}.
    $L^1_{loc}(I)$ is the space of locally $L^1(I)$ functions, i.e. integrable on any compact set $\subset I$.
\end{definition*}

\page{190}

\begin{remark}\ 

    For $a<c<d<b$, $f,g,pf^\prime, pg^\prime$ locally absolutely continuous on $I=(a,b)$, we have
    \begin{enumerate}[label = \roman*)]
        \item 
        \begin{equation*}
            \int_c^d\overline{g(y)}Af(y)r(y)\dd y = W_d(\overline{g}, f) - W_c(\overline{g}, f) + \int_c^d \overline{(Ag)}(y)f(y)r(y)\dd y
        \end{equation*}
        with $W_x(f_1, f_2) = \left(p\left(f_1f_2^\prime - f_1^\prime f_2\right)\right)(x)$ (``modified Wronskian'').
        \item Taking limits ($c\searrow a, d\nearrow b)$ and $f,g\in D(A)$ we find
        \begin{equation*}
            \inner{g}{Af} = W_b (\overline{g}, f) - W_a (\overline{g}, f) + \inner{Ag}{f}
        \end{equation*}
    \end{enumerate}
\end{remark}

\page{191}

\begin{lemma}\ 

    Every Sturm-Liouville equation can be transformed into one with $r=p\equiv 1$.
\end{lemma}

\page{192}

Classical differential equation: Suppose particle is moving on the half line $(0, \infty)$ in a potential $V\in C^1(0,\infty)$
s.t. $V^\prime$ is Lipschitz continuous on every compact interval. Classical equations of motion for position $x(t)$ and velocity $v(t)$:
\begin{equation}
    \dot x(t) = v(t),\quad \dot v(t) = -\frac{1}{m} V^\prime(x(t))\tag{*}\label{eq:pre-4.16}
\end{equation}
ODE-Theory $\implies$ For any $x_0>0, t_0>0, v_0\exists$ solution to \eqref{eq:pre-4.16} for $t$ \emph{near} $t_0$ with $x(t_0) = x_0, v(t_0) = v_0$.

\begin{remark}
    Suppose that $(x,v)$ is a solution for \eqref{eq:pre-4.16} with $x(t_0) = x_0>0, v(t_0) = v_0$
    such that the maximal interval on which this solution of the initial value problem exists is $[t_0, \tau)$ with $\tau < \infty$.
    Then either $\lim_{t\nearrow \tau} x(t) = 0$ or $\lim_{t\nearrow \tau} x(t) = \infty$ (``the particle runs into $0$ or $\infty$ in finite time'').
\end{remark}

\begin{notation*}
    The classical motion given by $V$ is called \underline{complete at} 0 (or $\infty$ resp.) if there is \emph{no} $(x_0, v_0)\in\R_{>0}\times\R$
    s.t. the solution to the initial value problem must run into 0 ($\infty$ resp.) in finite time
\end{notation*}

\page{193}

\begin{remark}
    $V\in C^1$ with Lipschitz continuity on any compact subset of $(0, \infty)$.
    Then the classical motion given by $V$ is:
    \begin{itemize}
        \item not complete at $0$ $\iff$ $V$ is bounded above near $0$
        \item not complete at $\infty$ $\iff$ $V$ is bounded above for $x\geq 1$ and $\int_1^\infty\frac{1}{\sqrt{C-V(x)}}\dd x < \infty$ for some $C>\sup V(x), x\geq 1$
    \end{itemize}
\end{remark}
\addtocounter{generalthm}{-1}

\begin{lemma}
    Let $V\in C(\R_{>0};\R), H = -\dv[2]{2} + V(x)$ on $L^2(\R_{>0},\dd x)$ with domain $D(H) = C^\infty_{0,\infty}(0,\infty)$
    ($C_{0,\infty}$ is supported away from 0 and $\infty$). Then 
    \begin{enumerate}[label = \roman*)]
        \item $H$ is symmetric
        \item If $u\in D(H^*)$, then $u$ is $C^1$, $u^\prime$ is absolutely continuous, 
        $U^{\prime\prime}\in L^2_{loc}, -u^{\prime\prime} + Vu \in L^2(\R_{>0})$ and $H^*u = -u^{\prime\prime} + Vu$
        \item\page{194} $H$ has equal deficiency indices
    \end{enumerate}
\end{lemma}

\begin{lemma}
    Let $F\in C(\R_{>0};\C)$. The set of solutions of $U^{\prime\prime} = Fu$ on $\R_{>0}$ is a 2-dimensional vectorspace of $C^2$-functions.
    \page{195}
    For any 2 solutions $u_1, u_2$ the Wronskian $W(u_1, u_2) \coloneqq u_1^\prime u_2 - u_1u_2^\prime$ is \emph{constant} and
    $W(u_1, u_2)\equiv 0 \iff u_1, u_2$ are linearly dependant.
\end{lemma}

\page{196}

\begin{lemma}
    Let $V\in C(\R_{>0}, \R)$ and consider 
    \begin{equation*}
        -u^{\prime\prime}(x) + v^\prime u(x) = \lambda u(x)\qquad\text{for }\lambda\in\C\tag{*}\label{eq:4.19}
    \end{equation*}
    Then:
    \begin{enumerate}[label =\roman*)]
        \item If $\Im\lambda\neq 0$ then at least one non-zero solution to \eqref{eq:4.19}
        is in $L^2$ near $0$ and at least one is in $L^2$ near $\infty$ 
        \item If for one $\lambda\in\C$, both solutions of \eqref{eq:4.19} are in $L^2$ near infinity (respectively near 0),
        then this is true for \emph{all} $\lambda\in\C$
    \end{enumerate}
\end{lemma}

\page{197}

\begin{notation*}
    $V$ is in the \underline{limit circle case} at $\infty$ (resp. at 0) if for some, hence for all $\lambda\in\C$,
    all solutions of \eqref{eq:4.19} are $L^2$ at $\infty$.
    $V$ is in the \underline{limit point case} at $\infty$ (resp. at 0) if it is not in the limit circle case at $\infty$  (resp. at 0).
\end{notation*}

\begin{theorem}
    Let $V\in C(\R_{>0}, \R)$. Then $H = -\dv[2]{x} + V(x)$ is essentially self-adjoint on $C_{0,\infty}^\infty(0,\infty)$ 
    $\iff V$ is in the limit point case oat both 0 and $\infty$
\end{theorem}

