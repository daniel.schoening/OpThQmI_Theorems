\lecture{21.12.21 21:15}{}
\file{17_OpThQM_pre.pdf}
\page{127}

\section*{Self-adjoint extensions}
\addcontentsline{toc}{section}{Self-adjoint extensions}

\begin{theorem}\label{th:3.26}
    Let $(D(T), T)$ be a closed symmetric operator on $\mathcal{H}$. Then:
    \begin{enumerate}[label =\roman*)]
        \item\label{th:3.26i} $\dim{\ker(\lambda - T^*)}$ is constant for all $\lambda \in \mathds{H}_+ = \set{z}{\,\mathrm{Im}(z) > 0}$ and
        $\dim{\ker(\lambda - T^*)}$ is constant for all $\lambda \in \mathds{H}_- = \set{z}{\,\mathrm{Im}(z) < 0}$
        \item $\sigma(T)$ is one of the following
        \begin{enumerate}[label = or \alph*)]
            \item[\phantom{or }a)]\setcounter{enumii}{1} $\overline{\mathds{H}_+}$
            \item $\overline{\mathds{H}_-}$ 
            \item $\C$
            \item\label{th:3.26iid} a closed subset of $\R$.
        \end{enumerate}
        \item $T$ selfadjoint $\iff$ \hyperref[th:3.26iid]{ii)d)} holds.
        \item\label{th:3.26iv} $T$ selfadjoint $\iff$ \emph{both} dimensions in \ref{th:3.26i} are 0.
    \end{enumerate}
\end{theorem}

\page{130}

\begin{corollary*}
    A closed, symmetric operator $T$, that has at least one \emph{real} number in $\rho(T)$ is selfadjoint.
\end{corollary*}

\begin{notation*}
    For $T$ symmetric, define
    \begin{gather*}
        n_+ \coloneqq \dim{\ker(i-T^*)}\quad (=\dim{\range{(i+T)}}\orth)\\
        n_- \coloneqq \dim{\ker(-i-T^*)}\quad (=\dim{\range{(i-T)}}\orth)\\
        K_+ \coloneqq \ker(i-T^*)\qquad\qquad K_- \coloneqq \ker(-i-T^*)
    \end{gather*}
    as $n_\pm$ ``defect indices of $T$'' and $K_\pm$ as ``defect subspaces''.
\end{notation*}

\begin{remark*}\

    \begin{enumerate}[label = \roman*)]
        \item $(n_+, n_-)\in (\N_0\cup\zeroset, N_0\cup\zeroset)$ 
        \item
        $n_+(T) = \dim{\ker(\lambda - T^*)}\forall \lambda\in\mathds{H}_+$\\
        $n_-(T) = \dim{\ker(\lambda - T^*)}\forall \lambda\in\mathds{H}_-$\\
        by Theorem \ref{th:3.26}.
        \item $T$ closed, symmetric is selfadjoint $\iff n_+ = n_- = 0$ (by Theorem \hyperref[th:3.26iv]{3.26 iv)}).
    \end{enumerate}
\end{remark*}

\page{131}

\begin{theorem}\label{th:3.27}
    Let $T$ be closed, symmetric. The closed symmetric extensions of $T$ are in 1 to 1 correspondence with the partial isometries of $K_+$ to $K_-$.
    Concretely, for $V$ partial isometry from $K_+$ to $K_-$, i.e. $V: K_+\supset(\ker V)\orth \to \range V\subset K_-$ unitary,
    $T_V$ is the extension with:
    \begin{gather}
        D(T_V) \coloneqq \set{u + (I + V) w_+}{u\in D(T), w_+\in(\ker V)\orth}\\
        T_V (u + (I + V) w_+) = Tu + i  (I - V) w_+
    \end{gather}
    \begin{enumerate}[label = \roman*)]
        \item If $\dim{\ker V}\orth < \infty$, then $T_V$'s defect indices are $n_\pm(T_V) = n_\pm(T) - \dim{\ker V}\orth$.
        \item $T$ has selfadjoint extensions $\iff n_+(T) = n_-(T)$. In this case the 1 to 1 correspondence is between selfadjoint extensions and unitaries $K_+\to K_-$.
        \item If $n_+(T) = 0$, $n_-(T) \neq 0$ or $n_+(T) \neq 0$, $n_-(T) = 0$, then $T$ has no proper symmetric extensions (such $T$: ``maximal symmetric'').
    \end{enumerate}
\end{theorem}

\page{132}

\begin{definition*}\index{relatively symmetric set}\index{relatively closed set}\index{relatively orthogonal set}
    Let $T$ be symmetric, closed. Define on $D(T*)$ 
    \begin{equation*}
        [u,v]_T \coloneqq \inner{T^*u}{v} - \inner{u}{T^*v}.
    \end{equation*}
    If $X\subset D(T^*)$ subspace with $[\cdot,\cdot]_T\big|_{X\times X} = 0$ is called \underline{$T$-symmetric}.
    Consider also 
    \begin{equation*}
        \inner{u}{v}_T \coloneqq \inner{u}{v} + \inner{T^*u}{T^*v}
    \end{equation*}
    and call a subspace $X\subset D(T^*)$ \underline{$T$-closed} iff closed w.r.t. $\inner{\cdot}{\cdot}_T$\newline
    and two subspaces $X,Y\subset D(T^*)$ \underline{$T$-orthogonal} iff $\inner{x}{y}_T=0\forall x\in X,y\in Y$
\end{definition*}

\begin{lemma}
    Let $T$ be closed, symmetric. Then 
    \begin{enumerate}[label = \roman*)]
        \item The closed, symmetric extensions of $T$ are restrictions of $T^*$ to $T$-closed, $T$-symmetric subspaces of $D(T^*)$.
        \item $D(T), K_+, K_-$ are $T$-closed, mutually $T$-orthogonal subspaces of $D(T^*)$ and 
        \begin{equation*}
            D(T^*) = D(T)\oplus_T K_+ \oplus_T K_- 
        \end{equation*}
        \item\page{133} There is a 1 to 1 correspondence between $T$-closed, $T$-symmetric subspaces $X$ of $D(T^*)$ that contain $D(T)$ and the 
        $T$-closed, $T$-symmetric subspaces $Y$ of $K_+ \oplus_T K_-$, given by $X = D(T)\oplus_T y$
    \end{enumerate}
\end{lemma}

\newpage