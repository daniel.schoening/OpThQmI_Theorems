\lecture{18.01.22 12:15}{}
\file{20_OpThQM_pre.pdf}
\page{150}

\section*{Application of Kato-Rellich to atomic Hamiltonians}
\addcontentsline{toc}{section}{Application of Kato-Rellich to atomic Hamiltonians}

\begin{proposition}\label{th:3.37}
    Let $V\in L^2(\R^3) + L^\infty(\R^3)$, i.e. $V = V_1 + V_2$ with $V_1\in L^2(\R^n), V_2\in L^\infty(\R^3)$.
    Let $\overline{V} = V$ ($V$ real valued). 
    Then $-\Delta + V(x)$ ($V(x)$ is the multiplication op. $M_{V(x)}$) is essentially selfadjoint on $C_0^\infty(\R^3)$
    (and selfadjoint on $D_\mathrm{max} = D(H_0)$).
\end{proposition}

\page{151}

\begin{example*}
    $V(x) = -\frac{e}{\norm{x}}$ with $e\in\R_{>0}$ (electric charge), $-\Delta + V$ is essentially selfadjoint.
\end{example*}

\begin{example*}
    Let $\set{V_j}_{j=1,\ldots,M}$ be a finite family of real-valued, measurable functions with $V_j\in L^2(\R^3) + L^\infty(\R^3)$.
    We consider $-\Delta+ \sum_{j=1}^M V_j(y_j)$ on $L^2(\R^{3n})$ where each $V_j$ is the multiplication operator
    acting on \emph{one} of the $L^2$ spaces in $L^2(\R^{3n}) = L^2(\R^2)\otimes\ldots\otimes L^2(\R^3)$ (i.e. $y_j$ coordinates of $\R^3$).
    From \ref{th:3.37}: $-\Delta \sum V_j(Y_j)$ is essentially selfadjoint.
\end{example*}

\page{152}

\begin{remark*}
    There considerations can be generalized to more complicated Hamiltonians, if certain criteria on boundedness from below are satisfied.
\end{remark*}

\section*{Nelson's analytic vector Theorem}
\addcontentsline{toc}{section}{Nelson's analytic vector Theorem}
\page{153}

% \begin{idea*}
%     Use Stones 
% \end{idea*}

\begin{definition*}\index{smooth verctor for an operator}\index{analytic verctor for an operator}
    Let $A$ be an operator on $\mathcal{H}$. Denote by $D^\infty(A) \coloneqq \bigcap_{n=1}^N\infty A^n$ the ``space of \underline{smooth vectors of $A$}''.
    $v\in C^\infty(A)$ is called \underline{analytic vector for $A$} iff $\sum_{n=0}^\infty\frac{\norm{A^nv}}{n!}t^n<\infty$ for some $t>0$.
\end{definition*}

\page{154}

\begin{definition*}\index{vector of uniqueness}
    Let $A$ be symmetric. Let $v\in D^\infty(A)$ and define $D_v \coloneqq \spn\set{A^nv}{n\in\N_0}$ and $\mathcal{H}_v = \overline{D_v}$
    Set $A_v: D_v\to D_v, A_v = A\big|_{D_v}, A_v\left(\sum_{n=0}^N c_nA^nv\right) = \sum_{n=0}^N c_nA^{n+1}v$.
    $v$ is called a \underline{vector of uniqueness} of $A$ iff $A_v$ is essentially selfadjoint on $D_v\subset\mathcal{H}_v$ as on operator on $\mathcal{H}_v$
\end{definition*}

\begin{definition*}\index{total set}
    A set of vectors $S$ in a vector space $V$ is called a \underline{total set} in $V$ iff $\spn S$ is dense in $V$. 
\end{definition*}

\begin{lemma}[Nussbaum]
    Let $A$ be symmetric. Let $D(A)$ contain a total set of vectors of uniqueness of $A$. Then $A$ is essentially selfadjoint.
\end{lemma}

\page{155}

\begin{theorem}[Nelsons's Analytic Vector Theorem]\index{Nelsons's Analytic Vector Theorem}
    Let $A$ be a symmetric operator on $\mathcal{H}$. If $D(A)$ contains a total set of analytic vectors, then $A$ is essentially selfadjoint.
\end{theorem}

\page{158}

\begin{example*}\ 

    Let $\mathcal{H} = L^2([0,1])$, $D(A) = \set{u\in\mathcal{H}}{u\text{ smooth, periodic with all derivatives periodic}}$, $A = -i\dv{x}$.
    A is symmetric (integration by parts) and for $e_k(X) = e^{2\pi ikx}\in D(A)$, $\spn\set{e_k}{k\in\Z}$ is dense in $\mathcal{H}$.
    We have 
    \begin{equation*}
        Ae_k = 2\pi k e_k\implies\sum_{n=0}^\infty \frac{\norm{A^ne_k}}{n!}t^n = \sum_{n=0}^\infty \frac{(2 \pi\abs{k})^n}{n!}t^n = e^{2\pi kt}<\infty
    \end{equation*}
    $\implies A$ is essentially selfadjoint.
\end{example*}

\begin{definition*}
    The Schwartz space $\mathcal{S}$ is the function space of all functions whose derivatives are rapidly decreasing.
    $\mathcal{S} \coloneqq \set{f\in C^\infty}{\forall\alpha,\beta\in\N^n, \sup_{x\in\R^n}\set{\abs{x^\alpha\left(D^\beta f\right)(x)}}<\infty}$ is the definition.
\end{definition*}

\begin{example*}\index{Hermite functions}
    $X$ is essentially selfadjoint on any subspace $\subset \set{u\in L^2(\R)}{Xu\in L^2(\R)}$ which contains the Hermite functions.
    Consider $A = \frac{1}{\sqrt{2}}\left(x + \dv{x}\right)$ and $A^\dagger = \frac{1}{\sqrt{2}}\left(x + \dv{x}\right)$ on the Schwartz Space $\mathcal{S}$.
    Let $h_0(x) = \frac{1}{2\sqrt{\pi}} e^{\frac{-x^2}{2}}, h_n = \frac{1}{\sqrt{n!}} \left(A^\dagger\right)^n h_0(x)$
    denote the \underline{Hermite functions}.
    $\set{h_n}{n\in\N_0}$ is an ONB for $L^2(\R)$. We have for $N \coloneqq A^\dagger$, $Nh_n = nh_n$ and $AA^\dagger - A^\dagger A = I$.
    \page{159}
    Moreover, $A^\dagger h_n = \sqrt{n+1}h_{n+1}\forall n$ (by def'n) and $A h_n = \sqrt{n}h_{n-1}$ for $n\geq 1$ with $Ah_0 = 0$.
    \begin{align*}
        \implies& \text{ for } A^\# = A\text{ or }A^\dagger:\\
        &\qquad\norm{\smash[b]{\underbrace{A^\#\ldots A^\#}_{\text{k times}}}h_0} \leq \sqrt{n+1}\ldots\sqrt{n+k} \leq \sqrt{(n+k)!}\\[1ex]
        \implies& \norm{x^k h_n}\leq 2^{\frac{k}{2}}\sqrt{(n+k)!}\quad\left(\text{using } x^k = 2^{-\frac{2}{k}}\left(A + A^\dagger\right)^k\right)\\
        \implies& \sum_{k=0}^\infty\frac{\norm{x^k h_n}}{k!}t^k \leq \sum_{k=0}^\infty\frac{2^{\frac{k}{2}}\sqrt{(n+k)!}}{k!}t^k<\infty\\
        \implies& h_n\text{ are analytic vectors for } X\\
        \implies& \text{ claim }\left(\text{since }X:\spn\set{h_n}\to\spn\set{h_n}\right) 
    \end{align*}
\end{example*}