\stepcounter{chapter}
\chapter{Unbound Operators}

\lecture{07.12.21 21:15}{}
\file{13_OpThQM_pre.pdf}
\page{94}

\begin{propositiondef}[Closed Graph Theorem]\label{prop:Closed Graph Theorem}\index{Closed Graph Theorem}
    Let $T: X\to Y$ be linear ($X$ and $Y$ Banach spaces). Then $T$ is bounded ($\opnorm{T}<\infty$) i.e. continuous
    $\iff$ The \underline{graph} $\Gamma(T)\coloneqq \set{(x,y)\in X\times Y}{y = Tx}$ is closed.
\end{propositiondef}

\page{95}

\begin{remark*}\label{remark:inner cross}
    For $X, Y$ Hilbert spaces, we have the scalar product $\inner{(u,v)}{(\tilde u, \tilde v)}_{X\times Y} = \inner{u}{\tilde u}_X + \inner{v}{\tilde v}_Y\forall u,\tilde u\in X, v,\tilde v\in Y$
\end{remark*}

\begin{corollary}[Helling-Toeplitz-Theorem]\label{coroll:Helling-Toeplitz-Theorem}\index{Helling-Toeplitz-Theorem}
    If $A:\mathcal{H}\to\mathcal{H}$ linear satisfies $\inner{u}{Av} = \inner{Au}{v}\forall u,v\in\mathcal{H}$, i.e. it is selfadjoint, then $A$ is bounded.  
\end{corollary}

\page{96}

\begin{definition}\index{closed operator}
    A (general) operator on a Hilbert space $\mathcal{H}$ is a tuple $(D(A), A)$ where $A:D(A)\to\mathcal{H}$ linear.
    We usually assume $D(A)$ is dense in $\mathcal{H}$. An operator $(D(A), A)$ is called \underline{closed} if its graph
    $\Gamma(A) = \set{(u,v)}{u\in D(A), v = Au}$ is closed in $\mathcal{H}\times\mathcal{H}$ 
    (w.r.t. scalar product in previus \hyperref[remark:inner cross]{remark}).\newline
    Careful: Since $D(A) \neq \mathcal{H}$ in general, Theorem \ref{prop:Closed Graph Theorem} and \ref{coroll:Helling-Toeplitz-Theorem}
    are in general not applicable. In fact, there are closed unbound operators.
\end{definition}

\begin{definition}\index{extension of operator}
    Let $(D(T_1), T_1), (D(T_2), T_2)$ be operators on $\mathcal{H}$. If $\Gamma(T_1)\supset\Gamma(T_2)$,
    then $T_1$ is an \underline{extension} of $T_2$ (write $T_1\supset T_2$).
\end{definition}

\begin{remark*}
    $\Gamma(T_1)\supset\Gamma(T_2) \iff D(T_1)\supset D(T_2)$ and $T_1u = T_2u\forall u\in D(T_2).$
\end{remark*}

\begin{definition}\index{closable operator}\index{closure of operator}
    $(D(T), T)$ is \underline{closable} iff it has closed extension.
    Every closable operator has a smallest closed extension, called its \underline{closure}, denoted $\overline{T}$.
\end{definition}

\page{97}

\begin{remark*}
    Not every operator is closable. The problem is that $\overline{\Gamma(T)}$ is not necessarily the graph of an operator.
\end{remark*}

\begin{proposition}
    IT $T$ is closable, then $\Gamma(\overline{T}) = \overline{\Gamma(T)}$.
\end{proposition}

\page{99}

\begin{definition}\index{densely defined operator}\index{adjoint of unbounded operator}
    Let $(D(T), T)$ be an operator on $\mathcal{H}$ with $D(T)$ dense in $\mathcal{H}$, also called \underline{$T$ densely defined} in $\mathcal{H}$.
    Let 
    \begin{equation*}
        D(T^*) \coloneqq \set{v\in\mathcal{H}}{\exists w\in\mathcal{H}\st\inner{Tu}{v} = \inner{u}{w}\forall u\in D(T)}.
    \end{equation*}
    For each such $v$, we define $T^*v \coloneqq w$ and $(D(T^*), T^*)$ is called the \underline{adjoint} of $T$.
\end{definition}

\begin{remark*}\ 

    \begin{enumerate}[label = \roman*)]
        \item $T^*$ is well-defined, since, by the denseness of $D(T)$ in $\mathcal{H}$, $w$ is uniquely determined (if its exists, there is only one).
        \item Riesz' lemma $\implies\left( v\in D(T) \iff \abs{\inner{Tu}{v}}\leq C\abs{u}\forall u\in D(T)\right)$
        \item $T\subset S \implies S^* \subset T^*$
        \item $D(T^*)$ can be small (in particular not dense even if $D(T)$ is)
    \end{enumerate}
\end{remark*}

\page{100}

\begin{theorem}
    Let $T$ be a densely defined operator on $\mathcal{H}$. Then 
    \begin{enumerate}[label = \roman*)]
        \item $T^*$ is closed
        \item $T$ is closable $\iff D(T^*)$ is dense. In this case, $\overline{T} = T^{**}$.
        \item If $T$ is closable, then $\overline{T}^* = T^*$.
    \end{enumerate}
\end{theorem}