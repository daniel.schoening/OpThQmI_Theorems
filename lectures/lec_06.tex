\lecture{12.11.21 12:15}{}
\file{06_OpThQM_pre.pdf}
\page{37}

\begin{example}\index{integral operator}\index{Hilbert-Schmitt kernel}
    Let $\mathcal{H} = L^2(X,\mu)$, let $A$ be an \underline{integral operator},
    i.e. $\exists a\colon X\times X\to\C$ measurable s.t. $(Au) = \int_X a(x,y) f(y) \mu(\dd y)$ defines a linear map $A\colon\mathcal{H}\to\mathcal{H}$.
    Then $A\in \mathcal{B}_2(\mathcal{H}) \iff a$ is a \underline{Hilbert-Schmitt kernel}, i.e. $\int_{X\times X} \abs{a(x,y)}^2 \mu(\dd x)\mu(\dd y)$,
    which is equivalent to $a\in L^2(X\times X, \mu\times\mu)$.\newline
    In this case $\hsnorm{A} = \norm{a}_{L^2(X\times X)}$.
\end{example}

\page{38}

\begin{definition*}\index{trace}\index{Hilbert-Schmitt scalar product}
    For $A, B\in \mathcal{B}_2(\mathcal{H})$ with ${e_k}_k$ as ONB of $\mathcal{H}$, set 
    \begin{equation*}
        \inner{A}{B} \coloneqq \sum_k\inner{Ae_k}{Be_k} \eqqcolon \trace{(A^*B)}.
    \end{equation*}
    This is the scalar product inducing the Hilbert-Schmitt norm.
\end{definition*}

\page{39}

\begin{remark}\index{trace class operator}\

    \begin{itemize}
        \item For $A,B\in \mathcal{B}_2(\mathcal{H}), \inner{A}{B}<\infty$.
        \item Trace class operators: $\exists A, B \in \mathcal{B}_2(\mathcal{H})\st C = AB$
        then $C$ is called a trace class operator. The space of the trace class operators $\mathcal{B}_1(\mathcal{H})$ is a two-sided ideal.
    \end{itemize}
\end{remark}

\section*{Elements of spectral theory for $\mathcal{B}(\mathcal{H})$}
\addcontentsline{toc}{section}{Elements of spectral theory for \texorpdfstring{$\mathcal{B}(\mathcal{H})$}{B(H)}}

\begin{definition}[spectrum]\index{spectrum of a bounded operator}
    The \underline{spectrum} of $T\in \mathcal{B}(\mathcal{H})$ is defined as
    \begin{equation*}
        \sigma(T) \coloneqq \set{\lambda\in\C}{T-\lambda\text{ is \underline{not} invertible in } \mathcal{B}(\mathcal{H})}
    \end{equation*}
\end{definition}

\begin{remark}[Bounded Inverse Theorem]\index{Bounded Inverse Theorem}
    Let $T:V_1\to V_2,\ V_1,V_2$ Banach spaces be bounded ($\opnorm{T}<\infty$) and bijective, then $T^{-1}$ is bounded.\newline
    Note: this is far from obvious. Needs the completeness of Banach spaces.
\end{remark}

\page{40}

\begin{definitions*}\
    
    \underline{Point spectrum}\index{point spectrum} or \underline{Eigenvalues}\index{eigenvalues} of $T$:
    \begin{equation*}
        \sigma_p(T) \coloneqq \set{\lambda\in\C}{T-\lambda\text{ is not injective}}.
    \end{equation*}
    \underline{Continuous spectrum}\index{continuous spectrum} of $T$:
    \begin{equation*}
        \sigma_c(T) \coloneqq \set{\lambda\in\C}{T-\lambda\text{ is injective and }\range(T-\lambda)\text{ is dense in }\mathcal{H}}.
    \end{equation*}
    \underline{Rest spectrum}\index{rest spectrum} of $T$:
    \begin{equation*}
        \sigma_r(T) \coloneqq \set{\lambda\in\C}{T-\lambda\text{ is injective and }\range(T-\lambda)\text{ is not dense in }\mathcal{H}}.
    \end{equation*}
\end{definitions*}

\begin{definitions*}\

    \underline{Resolvent set}\index{resolvent set} of $T$:
    \begin{equation*}
        \rho(T) \coloneqq \C\setminus\sigma(T) = \set{\lambda\in\C}{T - \lambda\text{ is invertible in }\mathcal{H}}.
    \end{equation*}
    \underline{Resolvent} of $T$ at $\lambda\in\rho(T)$:
    \begin{equation*}
        R(\lambda, T)\coloneqq -(T-\lambda)^{-1}\in \mathcal{B}(\mathcal{H}).
    \end{equation*}
\end{definitions*}

\newpage

\page{41}

\begin{proposition}
    Let $T\in \mathcal{B}(\mathcal{H})$.
    \begin{enumerate}[label = \roman*)]
        \item $\abs{\lambda}>\opnorm{T}$, then $\lambda\in \rho(T)$ and $R(\lambda, T) = \sum_{k=0}^\infty \lambda^{-k-1}T^k$ (converges in norm).
        \item $\lambda_0\in\rho, r\coloneqq\opnorm{R(\lambda_0, T)}^{-1}$, then $B_r(\lambda_0)\subset\rho(T)$ and in particular $\rho(T)$ is open.
        Furthermore, for $\abs{\lambda - \lambda_0}<r\colon R(\lambda, T) = \sum_{k=0}^\infty (\lambda_0 - \lambda)^k R(\lambda_0, T)^{k+1}$
        \item For $u,v\in\mathcal{H}, f(\lambda)\coloneqq \inner{v}{R(\lambda, T)u}$ defines a holomorphic function on $\rho$ with $\lim_{\abs{\lambda}\to\infty}f(\lambda)\to 0$
    \end{enumerate}
\end{proposition}

\page{43}

\begin{theorem}
    Let $T\in \mathcal{B}(\mathcal{H})$, then $\sigma(T)\neq\emptyset, \sigma(T)$ is compact in $\C$ and $\sigma(T)\subset\set{z\in\C}{\abs{z}\leq\opnorm{T}}$.
\end{theorem}

\begin{definitions*}[spectral radius]\index{spectral radius}
    The \underline{spectral radius} is defined as $r(T) \coloneqq \max\set{\abs{\lambda}}{\lambda\in\sigma(T)}$.
\end{definitions*}

\begin{remark*}
    For the spectral radius we have $r(T)\leq\opnorm{T}$ with $T = T^* \implies r(T)=\opnorm{T}$.
\end{remark*}

\page{44}

\begin{remark}
    Let $T\in \mathcal{B}(\mathcal{H})$. Then
    \begin{enumerate}[label = \roman*)]
        \item $\lambda\in\sigma(T)\iff\overline{\lambda}\in\sigma(T^*)$
        \item $\lambda\in\sigma_c(T)\iff\overline{\lambda}\in\sigma_c(T^*)$
        \item $\lambda\in\sigma_r(T)\implies\overline{\lambda}\in\sigma_p(T^*)$\newline
        Careful: for $\lambda\in\sigma_p(T), \overline{\lambda}$ could be in $\sigma_p(T^*)$ or in $\sigma_r(T^*)$.
    \end{enumerate}
\end{remark}

\page{45}

\begin{corollary}
    Let $T\in \mathcal{B}(\mathcal{H}), T = T^*$, then $\sigma(T)\in\R$.
\end{corollary}